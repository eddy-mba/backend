import { MigrationInterface, QueryRunner } from "typeorm";

export class Migrate1705164611445 implements MigrationInterface {
    name = 'Migrate1705164611445'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "noticias" DROP COLUMN "fecha"`);
        await queryRunner.query(`ALTER TABLE "noticias" ADD "fecha" date NOT NULL DEFAULT CURRENT_DATE::date`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "noticias" DROP COLUMN "fecha"`);
        await queryRunner.query(`ALTER TABLE "noticias" ADD "fecha" TIMESTAMP NOT NULL DEFAULT ('now'::text)::date`);
    }

}
