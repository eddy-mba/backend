import { MigrationInterface, QueryRunner } from "typeorm";

export class Migrate1705245244234 implements MigrationInterface {
    name = 'Migrate1705245244234'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "noticias" ALTER COLUMN "fecha" SET DEFAULT ('now'::text)::date`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "noticias" ALTER COLUMN "fecha" SET DEFAULT CURRENT_DATE`);
    }

}
