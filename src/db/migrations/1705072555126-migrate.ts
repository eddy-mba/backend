import { MigrationInterface, QueryRunner } from "typeorm";

export class Migrate1705072555126 implements MigrationInterface {
    name = 'Migrate1705072555126'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "noticias" ("id" SERIAL NOT NULL, "titulo" character varying NOT NULL, "imagen" character varying, "fecha" TIMESTAMP NOT NULL DEFAULT ('now'::text)::date, "lugar" character varying NOT NULL, "autor" character varying NOT NULL, "contenido" character varying NOT NULL, CONSTRAINT "PK_526a107301fc9dfe8d836d6cf27" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "noticias"`);
    }

}
