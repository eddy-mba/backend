import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Noticias } from './noticias.entity';
import { ILike, Repository } from 'typeorm';
import { PostNoticiaDto } from './dto/post.dto';
import { UpdateNoticiaDto } from './dto/update.dto';

@Injectable()
export class NoticiasService {
  constructor(
    @InjectRepository(Noticias)
    private noticiasRepository: Repository<Noticias>,
  ) {}

  postNoticia(body: PostNoticiaDto) {
    const newNoticia = this.noticiasRepository.create(body);
    return this.noticiasRepository.save(newNoticia);
  }

  getNoticias() {
    return this.noticiasRepository.find();
  }

  getNoticia(id: number) {
    return this.noticiasRepository.findOne({ where: { id: id } });
  }

  updateNoticia(id: number, body: UpdateNoticiaDto) {
    return this.noticiasRepository.update(id, body);
  }

  deleteNoticia(id: number) {
    return this.noticiasRepository.delete(id);
  }
  async buscarTitulo(titulo: string) {
    const buscar = await this.noticiasRepository.find({
      where: {
        titulo: ILike(`%${titulo}%`),
      },
    });
    if (!buscar.length) {
      throw new NotFoundException(
        'No se encontraron entidades con palabras similares.',
      );
    }
    return buscar;
  }
}
