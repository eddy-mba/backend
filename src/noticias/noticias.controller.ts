import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
} from '@nestjs/common';
import { NoticiasService } from './noticias.service';
import { PostNoticiaDto } from './dto/post.dto';
import { UpdateNoticiaDto } from './dto/update.dto';

@Controller('noticias')
export class NoticiasController {
  constructor(private noticiasServer: NoticiasService) {}

  @Post()
  postNoticia(@Body() newNoticia: PostNoticiaDto) {
    return this.noticiasServer.postNoticia(newNoticia);
  }

  @Get()
  getNoticias() {
    return this.noticiasServer.getNoticias();
  }
  @Get(':id')
  getNoticia(@Param('id', ParseIntPipe) id: number) {
    return this.noticiasServer.getNoticia(id);
  }

  @Put(':id')
  updateNoticia(
    @Param('id', ParseIntPipe) id: number,
    @Body() body: UpdateNoticiaDto,
  ) {
    return this.noticiasServer.updateNoticia(id, body);
  }

  @Delete(':id')
  deleteNoticia(@Param('id', ParseIntPipe) id: number) {
    return this.noticiasServer.deleteNoticia(id);
  }

  @Get('buscar/:titulo')
  buscarText(@Param('titulo') titulo: string) {
    return this.noticiasServer.buscarTitulo(titulo);
  }
}
