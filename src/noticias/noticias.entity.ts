import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Noticias {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  titulo: string;

  @Column({ nullable: true })
  imagen: string;

  @Column({ type: 'date', default: () => 'CURRENT_DATE' })
  fecha: Date;

  @Column()
  lugar: string;

  @Column()
  autor: string;

  @Column()
  contenido: string;
}
