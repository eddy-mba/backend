export  class PostNoticiaDto {
  titulo: string;
  imagen: string;
  lugar: string;
  autor: string;
  contenido: string;
}
