export class UpdateNoticiaDto {
  titulo?: string;
  imagen?: string;
  lugar?: string;
  autor?: string;
  contenido?: string;
}
