# El Hocicón Backend

## Descripción del Proyecto

Este es el repositorio del backend para el portal de noticias "El Hocicón".
Utiliza NodeJS, NestJS, TypeORM y PostgreSQL para gestionar y proporcionar datos al frontend.

## Tecnologías Utilizadas

- NodeJS v20
- NestJS v10
- TypeORM v0.3
- PostgreSQL v16

## Estructura del Proyecto

- `/src/noticias`: Contiene el código fuente de la aplicación.

  - `/dto`: Contiene objetos de transferencia de datos.
  - `/controllers`: Controladores de NestJS.
  - `/entities`: Entidades de TypeORM.
  - `/services`: Servicios de NestJS.
  - `/module`: modulos de NestJS.

- `/src/db/migrations`: Migraciones de base de datos.

  Nest is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them, please [read more here](https://docs.nestjs.com/support).

## Stay in touch

- Author - [Kamil Myśliwiec](https://kamilmysliwiec.com)
- Website - [https://nestjs.com](https://nestjs.com/)
- Twitter - [@nestframework](https://twitter.com/nestframework)

## License

Nest is [MIT licensed](LICENSE).
