# Instrucciones de Instalación y Despliegue del Backend

## Requisitos Previos

Asegúrate de tener las siguientes herramientas instaladas en tu máquina antes de proceder:

- [Node.js](https://nodejs.org/) (v20 o posterior)
- [PostgreSQL](https://www.postgresql.org/) (v16 o posterior)
- [Git](https://git-scm.com/)

## Instalación

1.  **Clonar este Repositorio:**

    ```bash
    git clone git@gitlab.com:eddy-mba/backend.git

    ```

2.  **Ingresar al Directorio del Backend:**

    ```bash
    cd backend

    ```

3.  **Instalar Dependencias:**

    ```bash
    npm install

    ```

4.  **Configurar Variables de Entorno:**
    Copia el archivo de configuración .env.example a .env.
    Abre el archivo .env y configura las variables de entorno según sea necesario.

5.  **Ejecutar Migraciones:**

    ```bash
    npm run migration:generate
    npm run migration:run

    ```

    Esto aplicará las migraciones necesarias a la base de datos.

## Instrucciones de Desarrollo

    ```bash
      npm run start:dev

    ```
    El servidor estará disponible en http://localhost:3020.

## despliegue en Render

Una forma facil de desplegar la aplicacion NestJS en en la plataforma [Render](https://render.com/).
Consulte para mas detalles la [documentacion de web-services](https://docs.render.com/web-services).
